#!/usr/bin/python3

""" Import modules """
import os
import yaml
import netmiko
import paramiko
import argparse
from datetime import datetime
from dotenv import dotenv_values
from colorama import Fore, Style

# Argument definitions
parser = argparse.ArgumentParser(description='For authorized Spigos only',
                                 formatter_class=argparse.RawDescriptionHelpFormatter,
                                 epilog='''
Spigo Script helps you to issue commands on many devices.
Every output is saved on txt file named as the device hostname.
If you prefer to have all the outputs in one file use -f option
followed by a file name.
Devices data must be entered in the devices.yaml file.
Command entry is prompted during execution.
''')
#parser.add_argument('-s', '--single', type=str, help='Run single command', required=False)
#parser.add_argument('-m', '--multi', type=list, help='Run multiple commands separated by comma', required=False)
parser.add_argument('-f', '--filename', type=str, help='Specify a single destination file for all output', required=False)
args = parser.parse_args()


### Control functions ###


def create_folder(folder):
    """function to check if folder exist and create it"""
    try:
        os.makedirs(folder)
        colored_green(str(folder) + " folder: created")
    except FileExistsError:
        colored_yellow(str(folder) + " folder: present")


def load_login_info(file):
    """function to load login info from .env file or from environment variables"""
    if os.path.exists(file) == True:
        credentials = dotenv_values(file)
        colored_yellow("credentials: loaded")
        return credentials
    else:
        credentials = {
            "DEVICES_CREDS_USR": os.getenv("DEVICES_CREDS_USR"),
            "DEVICES_CREDS_PSW": os.getenv("DEVICES_CREDS_PSW"),
        }
        return credentials


def yaml_to_json(file):
    """function to convert yaml inventory to json inventory"""
    with open(file, "r", encoding="utf8") as stream:
        try:
            parsed_yaml = yaml.safe_load(stream)
            colored_yellow("inventory: loaded")
            return parsed_yaml
        except yaml.YAMLError as exc:
            print(exc)


def add_login_info(inventory):
    """Function to add credentials on device inventory"""
    for item in inventory:
        if item["username"] is None and item["password"] is None:
            item["username"] = credentials.get("DEVICES_CREDS_USR")
            item["password"] = credentials.get("DEVICES_CREDS_PSW")
        else:
            pass
    return inventory


def conf_to_file(folder, file, data):
    """Function write conf to file"""
    with open(folder + file, "a", encoding="utf8") as log_file:
        log_file.write(data)
        log_file.write("\n")


def summary_report(data):
    """Function to log summary report"""
    with open("summary.log", "a", encoding="utf8") as log_file:
        log_file.write(data)
        log_file.write("\n")


def clear_summary_report():
    """Function to clear log file"""
    with open("summary.log", "w", encoding="utf8") as summary_file:
        summary_file.close()
        colored_yellow("summary.log: cleared")


def exception_management(net_device, exception_list):
    """Function to print only one error in case of exception"""
    if exception_list == ['TE', 'TE']:
        output = (
            net_device["host"] + "_" + net_device["ip"]
            + ": ERROR [Device unreachable]"
        )
        colored_red(output)
        summary_report(output)
    elif exception_list == ['AE', 'AE']:
        output = (
            net_device["host"] + "_" + net_device["ip"]
            + ": ERROR [Authentication failed]"
        )
        colored_red(output)
        summary_report(output)
    else:
        pass


def colored_green(text):
    """Green color text"""
    print(Fore.GREEN + text + Style.RESET_ALL)


def colored_red(text):
    """Red color text"""
    print(Fore.RED + text + Style.RESET_ALL)


def colored_yellow(text):
    """Yellow color text"""
    print(Fore.YELLOW + text + Style.RESET_ALL)



### Connection functions ###


def SendCommand(net_device, commands):
    """function send commands"""
    exception_list = []
    for n in range(2):
        try:
            net_connect = netmiko.ConnectHandler(**net_device)
            # print("Working on " + net_device['host'])
            folder = "./output/"
            if args.filename is None:
                filename = net_device["host"] + "_" + net_device["ip"] + ".txt"
                for command in commands:
                    result = net_connect.send_command(command, expect_string=r"#", read_timeout=120)
                    conf_to_file(folder, filename, result)
            else:
                filename = args.filename
                conf_to_file(folder, filename, "\n" + "########## " + net_device["host"] + " " + net_device["ip"] + " ##########" + "\n")
                for command in commands:
                    result = net_connect.send_command(command, expect_string=r"#", read_timeout=120)
                    #conf_to_file(folder, filename, "\n" + "########## " + net_device["host"] + " ##########" + "\n" + result)
                    conf_to_file(folder, filename, result + "\n\n")
            output = net_device["host"] + "_" + net_device["ip"] + ": OK"
            colored_green(output)
            summary_report(output)
        except netmiko.NetmikoTimeoutException:
            exception_list.append('TE')
            continue
        except netmiko.NetmikoAuthenticationException:
            exception_list.append('AE')
            continue
        else:
            break
    exception_management(net_device, exception_list)


########################

# start data provisioning
print(Style.BRIGHT + "\n*** PROVISIONING ***" + Style.RESET_ALL)

# clear summary.log file
clear_summary_report()

# create output folder
create_folder("./output/")

# import inventory and convert to json
devices = yaml_to_json("devices.yaml")

# import credential from .env or env vars
credentials = load_login_info(".env")

# use default login info or override with specific
devices = add_login_info(devices)

# Commands input
#commands = input('Insert commands to run on devices: ')
commands = list(input('Insert commands to run on devices separated by a comma:\n -> ').split(","))

# start connection
print(Style.BRIGHT + "\n*** CONNECTING ***" + Style.RESET_ALL)

# connect to the device w/ netmiko
for device in devices:
    SendCommand(device, commands)

print("\n")
