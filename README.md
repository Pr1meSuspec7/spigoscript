# SpigoScript

### Requirements
This script tested on Linux/Windows with python3.7 or higher.  
The following packages are required:
 - pyyaml 
 - netmiko
 - python-dotenv
 - colorama

It's recommended to crate a virtual environment, activate it and then install the packages:

For Windows:

```sh
> git clone https://gitlab.com/Pr1meSuspec7/spigoscript.git
> cd spigoscript
> python -m venv venv-spigoscript
> venv-spigoscript\Scripts\activate.bat
> pip install pyyaml netmiko python-dotenv colorama
```

For Linux:

```sh
$ git clone https://gitlab.com/Pr1meSuspec7/spigoscript.git
$ cd spigoscript
$ python -m venv venv-spigoscript
$ source venv-spigoscript/bin/activate
$ pip install pyyaml netmiko python-dotenv colorama
```

### How it works

Create the devices.yaml file with your devices:
```sh
- host: "ASAv1"
  device_type: "cisco_asa"
  ip: "192.168.178.201"

- host: "ASAv2"
  device_type: "cisco_asa"
  ip: "192.168.178.202"
```

Create .env file with credentials:
```sh
DEVICES_CREDS_USR='admin'
DEVICES_CREDS_PSW='admin'
```

Run
```sh
$ python main.py
```

When prompted enter the command to be executed on the devices:
```sh
Insert commands to run on devices: show running | include crypto
```
